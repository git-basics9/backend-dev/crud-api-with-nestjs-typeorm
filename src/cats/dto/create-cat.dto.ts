import { IsNotEmpty, Length } from 'class-validator';

export class CreateCatDto {
  @IsNotEmpty({ message: 'Name cant be empty' })
  @Length(5, 255)
  name: string;

  @IsNotEmpty({ message: 'Breed cant be empty' })
  @Length(5)
  breed: string;
}
