// src/app.module.ts
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SqliteConnectionOptions } from 'typeorm/driver/sqlite/SqliteConnectionOptions';
import { CatsModule } from './cats/cats.module';

import { DataSource } from 'typeorm';
import { Cat } from './cats/cat.entity';

const config: SqliteConnectionOptions = {
  type: 'sqlite',
  database: 'data.db',
  // entities: [__dirname + '/../**/*.entity{.ts,.js}'],
  entities: [Cat],
  synchronize: true, //do not use this in production-> set false
};
@Module({
  imports: [CatsModule, TypeOrmModule.forRoot(config)],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
