import { Cat } from './cat.entity';
import { Repository } from 'typeorm';
import { CreateCatDto } from './dto/create-cat.dto';
import { UpdateCatDto } from './dto/update-cat.dto';
export declare class CatsService {
    private catsRepository;
    constructor(catsRepository: Repository<Cat>);
    create(catsData: CreateCatDto): Promise<Cat>;
    findAll(): Promise<Cat[]>;
    findOne(id: number): string;
    update(id: number, updateCatDto: UpdateCatDto): string;
    remove(id: number): string;
}
