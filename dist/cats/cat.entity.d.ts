export declare class Cat {
    id: number;
    name: string;
    breed: string;
    isAvailable: boolean;
}
